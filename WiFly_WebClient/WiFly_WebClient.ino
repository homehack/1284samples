
// (Based on Ethernet's WebClient Example)

#include <SPI.h>
#include <WiFly.h>


#include "Credentials.h"


byte server[] = { 66, 249, 89, 104 }; // Google

//Client client(server, 80);

WiFlyClient client;

void setup() {
  Serial1.begin(9600);
  Serial1.print("TEST123");

  Serial.begin(57600);
  Serial.println("WiFly Client test");
  //    pinMode(8,OUTPUT);    // power up the XBee socket
//  digitalWrite(8,HIGH);
  // lots of time for the WiFly to start up and also in case I need to stop the transmit
  delay(10000);
 
  Serial.println("Setting WiFly Port");
//  Serial1.begin(9600);
//  Serial1.print("TEST123");
  WiFly.setUart(&Serial1);
  
  Serial.println("Beginning WiFly");
  WiFly.begin();
//  WiFly.configure(WIFLY_BAUD, 9600);
  
  if (!WiFly.join(ssid, passphrase, mode)) {
//  if (!WiFly.join(ssid)) {
    Serial.println("Association failed.");
    while (1) {
      // Hang on failure.
    }
  }  

  Serial.println("connecting...");

  if (client.connect("google.com", 80)) {
    Serial.println("connected");
    client.println("GET /search?q=arduino HTTP/1.0");
    client.println();
  } else {
    Serial.println("connection failed");
  }
  
}

void loop() {
  if (client.available()) {
    char c = client.read();
    Serial.print(c);
  }
  
  if (!client.connected()) {
    Serial.println();
    Serial.println("disconnecting.");
    client.stop();
    for(;;)
      ;
  }
}


